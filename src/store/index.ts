import {createStore, combineReducers, applyMiddleware} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import user from '../store/user';
import {connectRouter, routerMiddleware} from 'connected-react-router'
import {createBrowserHistory} from 'history'
import ReduxThunk from 'redux-thunk';

export const history = createBrowserHistory();

//Reducer
const rootReducer = combineReducers({
  user,
  router: connectRouter(history)
});

export type RootState = ReturnType<typeof rootReducer>;


//Store
const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(routerMiddleware(history), ReduxThunk)
  )
);

export default store;