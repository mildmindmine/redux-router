import {
  UserAddAction,
  UserActionType,
  UserState,
  UserAction
} from './types';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '..';
import { push, CallHistoryMethodAction } from 'connected-react-router';

export function updateUser(user: UserState): UserAddAction {
  return {
    type: UserActionType.AddUser,
    payload: user,
  };
}

export function reset(): UserAddAction {
  return {
    type: UserActionType.ResetUser,
    payload: {
      firstName: "",
      lastName: "",
      nickName: ""
    },
  };
}


export function resetUser(): ThunkAction<void, RootState, void, UserAction| CallHistoryMethodAction> {
  return (dispatch, getState) => {
    console.log("resetUser");
    dispatch(reset());
    dispatch(push("/user"));
  }
}