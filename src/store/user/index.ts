import { UserAction, UserActionType, UserState } from './types';

const initialState: UserState = {
  firstName: 'test',
  lastName: 'hello',
  nickName: 'nick'
};

function userReducer(state = initialState, action: UserAction): UserState {
  switch (action.type) {
    case UserActionType.AddUser:
      return action.payload;
    case UserActionType.ResetUser:
      return action.payload;
    default:
      return state;
  }
}

export default userReducer;