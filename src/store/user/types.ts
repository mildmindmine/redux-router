export interface UserState {
  firstName: string;
  lastName: string;
  nickName: string;
}

export enum UserActionType {
  AddUser = 'User/AddUser',
  ResetUser = "User/Reset"
}

export interface UserAddAction {
  type: UserActionType.AddUser | UserActionType.ResetUser;
  payload: {
    firstName: string;
    lastName: string;
    nickName: string;
  };
}

export type UserAction = UserAddAction;