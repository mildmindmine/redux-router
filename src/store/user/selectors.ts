import { RootState } from '../../store';
import {UserState} from './types'

export function getUserObject(state: RootState): UserState {
  return {
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    nickName: state.user.nickName
  };
}