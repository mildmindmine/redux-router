import React from 'react';
import {AppBar, Typography, Toolbar, makeStyles, Link} from '@material-ui/core';
import {Link as RouterLink} from 'react-router-dom';
import {Switch, Route} from 'react-router';
import UserPage from './components/UserPage';
import Home from './components/Home'

const useStyles = makeStyles(theme => ({
  root: {
    flex: 1,
  },
  menucontainer: {
    display: 'flex',
    flexBasis: 1,
    flex: 1,
    justifyContent: 'flex-end',
  },
  navLink: {
    color: '#eee',
    textDecoration: 'none',
    marginLeft: theme.spacing(4),
    padding: theme.spacing(2),
  },
  activeNavLink: {
    color: 'black',
    backgroundColor: 'pink',
    textDecoration: 'none',
    fontSize: 20,
    marginLeft: theme.spacing(4),
    padding: theme.spacing(2),
    '& p ': {
      fontSize: 20
    },
  },
  navButton: {
    marginLeft: theme.spacing(4),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    color: 'black'
  }
}))

const App = () => {
  const classes = useStyles();
  return (
    <div>
      <AppBar position="static">
        <Toolbar className={classes.root}>
          <Typography>Redux Router</Typography>
          <div className={classes.menucontainer}>
            <Link component={RouterLink} to="/" underline="none" className={classes.navLink}>Home</Link>
            <Link component={RouterLink} to="/user" underline="none" className={classes.navLink}>User</Link>
          </div>
        </Toolbar>
      </AppBar>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/user" component={UserPage} />
      </Switch>
    </div>
  );
}
export default App;
