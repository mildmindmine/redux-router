import React from 'react';
import {Button} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import {resetUser} from '../store/user/action'

const Home: React.FC = () => {

  const dispatch = useDispatch();

  return <div>
    <h1>Home</h1>
    <Button variant="contained" color="primary" onClick={() => dispatch(resetUser())}>Reset State and Navigate to User</Button>
  </div>
}

export default Home;