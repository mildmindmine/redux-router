import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';
import { TextField, Typography, makeStyles, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import {getUserObject} from '../store/user/selectors';
import {updateUser} from '../store/user/action'

const useStyles = makeStyles(theme => ({
  formDiv: {
    display:'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(4)
  },
  formPaper: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '50%',
    height: '300px',
    padding: theme.spacing(4)
  }
}));

const UserPage: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userObj = useSelector(getUserObject);

  const [first, setFirst] = useState(userObj.firstName);
  const [last, setLast] = useState(userObj.lastName);
  const [nickName, setNickName] = useState(userObj.nickName);

  const onSubmit = () => {
    console.log("user", userObj);
    dispatch(updateUser(
      {
        firstName: first,
        lastName: last,
        nickName: nickName
      }
    ));
  }

  return <div className={classes.formDiv}>
    <Paper variant="outlined" className={classes.formPaper}>
      <Typography>Please input your information here</Typography>
      <TextField
        autoFocus 
        fullWidth 
        placeholder="First Name"
        value = {first}
        onChange = {event => {
          setFirst(event.target.value)
        }}
        margin="normal" 
        />
      <TextField
        fullWidth 
        placeholder="Last Name" 
        value = {last}
        onChange = {event => {
          setLast(event.target.value)
        }}
        margin="normal"/>
      <TextField
        fullWidth 
        placeholder="Nickname"
        value = {nickName}
        onChange = {event => {
          setNickName(event.target.value)
        }}
        margin="normal" />
      <Button 
        variant="contained"
        color="primary"
        onClick={onSubmit}
      >
        SUBMIT
      </Button>
    </Paper>
    <Paper variant="outlined" className={classes.formPaper}>
      <Typography>Your information</Typography>
      <Typography>First Name: {userObj.firstName}</Typography>
      <Typography>Last Name: {userObj.lastName}</Typography>
      <Typography>Nick Name: {userObj.nickName}</Typography>
    </Paper>
  </div>
}

export default UserPage;